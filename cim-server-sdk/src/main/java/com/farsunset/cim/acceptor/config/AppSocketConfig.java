/*
 * Copyright 2013-2022 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.farsunset.cim.acceptor.config;

import com.farsunset.cim.handler.CIMRequestHandler;


/**
 * TLV协议socket端口配置
 */
public class AppSocketConfig {

    private static final int DEFAULT_PORT = 23456;

    /*
    TVL协议socket端口
     */
    private Integer port;

    /**
     * socket消息处理器
     */
    private CIMRequestHandler outerRequestHandler;

    /**
    是否启用TVL协议socket
     */
    private boolean enable;

    public Integer getPort() {
        return port == null ? DEFAULT_PORT : port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public CIMRequestHandler getOuterRequestHandler() {
        return outerRequestHandler;
    }

    public void setOuterRequestHandler(CIMRequestHandler outerRequestHandler) {
        this.outerRequestHandler = outerRequestHandler;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
